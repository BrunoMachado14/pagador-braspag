﻿namespace PaymentCwi.Services
{
    using System;
    using Newtonsoft.Json.Linq;
    using PaymentCwi.Models;
    using PaymentCwi.Models.PaymentTypes;

    /// <summary>
    /// The payment converter.
    /// </summary>
    public class PaymentConverter : JsonCreationConverter<Payment>
    {
        /// <summary>The create.</summary>
        /// <param name="objectType">The object type.</param>
        /// <param name="jObject">The j object.</param>
        /// <returns>The <see cref="Payment"/>.</returns>
        public override Payment Create(Type objectType, JObject jObject)
        {
            var type = jObject.GetValue("type", StringComparison.OrdinalIgnoreCase);

            if (type == null) return null;

            switch (type.ToString().ToLower())
            {
                case "creditcard":
                    return new CreditCardPayment();
                case "debitcard":
                    return new DebitCardPayment();
                case "boleto":
                    return new BoletoPayment();
                case "eletronictransfer":
                    return new EletronicTransferPayment();
                default:
                    return null;
            }
        }
    }
}