﻿namespace PaymentCwi.Models.Antifraud
{
    /// <summary>
    /// The travel leg data.
    /// </summary>
    public class TravelLegData
    {
        /// <summary>
        /// Gets or sets the destination.
        /// </summary>
        public string Destination { get; set; }

        /// <summary>
        /// Gets or sets the origin.
        /// </summary>
        public string Origin { get; set; }
    }
}
