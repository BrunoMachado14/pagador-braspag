﻿namespace PaymentCwi.Models.Antifraud
{
    using PaymentCwi.Models.Enum;

    /// <summary>
    /// The passenger data.
    /// </summary>
    public class PassengerData
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the identity.
        /// </summary>
        public string Identity { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        public PassengerRatingEnum Rating { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get; set; }
    }
}