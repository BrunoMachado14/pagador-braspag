﻿namespace PaymentCwi.Models.Antifraud
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The travel data.
    /// </summary>
    public class TravelData
    {
        /// <summary>
        /// Gets or sets the route.
        /// </summary>
        public string Route { get; set; }

        /// <summary>
        /// Gets or sets the departure time.
        /// </summary>
        public DateTime? DepartureTime { get; set; }

        /// <summary>
        /// Gets or sets the journey type.
        /// </summary>
        public string JourneyType { get; set; }

        /// <summary>
        /// Gets or sets the legs.
        /// </summary>
        public List<TravelLegData> Legs { get; set; }
    }
}