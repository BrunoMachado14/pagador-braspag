﻿namespace PaymentCwi.Models.Antifraud
{
    /// <summary>
    /// The finger print data.
    /// </summary>
    public class FingerPrintData
    {
        /// <summary>
        /// Gets or sets the cookies enabled field.
        /// </summary>
        public string CookiesEnabledField { get; set; }

        /// <summary>
        /// Gets or sets the flash enabled field.
        /// </summary>
        public string FlashEnabledField { get; set; }

        /// <summary>
        /// Gets or sets the hash field.
        /// </summary>
        public string HashField { get; set; }

        /// <summary>
        /// Gets or sets the images enabled field.
        /// </summary>
        public string ImagesEnabledField { get; set; }

        /// <summary>
        /// Gets or sets the javascript enabled field.
        /// </summary>
        public string JavascriptEnabledField { get; set; }

        /// <summary>
        /// Gets or sets the proxy ip address field.
        /// </summary>
        public string ProxyIpAddressField { get; set; }

        /// <summary>
        /// Gets or sets the proxy ip address activities field.
        /// </summary>
        public string ProxyIpAddressActivitiesField { get; set; }

        /// <summary>
        /// Gets or sets the proxy ip address attributes field.
        /// </summary>
        public string ProxyIpAddressAttributesField { get; set; }

        /// <summary>
        /// Gets or sets the proxy server type field.
        /// </summary>
        public string ProxyServerTypeField { get; set; }

        /// <summary>
        /// Gets or sets the true ip address field.
        /// </summary>
        public string TrueIpAddressField { get; set; }

        /// <summary>
        /// Gets or sets the true ip address activities field.
        /// </summary>
        public string TrueIpAddressActivitiesField { get; set; }

        /// <summary>
        /// Gets or sets the true ip address attributes field.
        /// </summary>
        public string TrueIpAddressAttributesField { get; set; }

        /// <summary>
        /// Gets or sets the true ip address city field.
        /// </summary>
        public string TrueIpAddressCityField { get; set; }

        /// <summary>
        /// Gets or sets the true ip address country field.
        /// </summary>
        public string TrueIpAddressCountryField { get; set; }

        /// <summary>
        /// Gets or sets the smart id field.
        /// </summary>
        public string SmartIdField { get; set; }

        /// <summary>
        /// Gets or sets the smart id confidence level field.
        /// </summary>
        public string SmartIdConfidenceLevelField { get; set; }

        /// <summary>
        /// Gets or sets the screen resolution field.
        /// </summary>
        public string ScreenResolutionField { get; set; }

        /// <summary>
        /// Gets or sets the browser language field.
        /// </summary>
        public string BrowserLanguageField { get; set; }
    }
}