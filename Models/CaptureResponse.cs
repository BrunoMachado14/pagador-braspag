﻿namespace PaymentCwi.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// The capture response.
    /// </summary>
    public class CaptureResponse : BaseResponse
	{
	    /// <summary>
	    /// Gets or sets the status.
	    /// </summary>
	    public int Status { get; set; }

	    /// <summary>
	    /// Gets or sets the reason code.
	    /// </summary>
	    public int ReasonCode { get; set; }

	    /// <summary>
	    /// Gets or sets the reason message.
	    /// </summary>
	    public string ReasonMessage { get; set; }

	    /// <summary>
	    /// Gets or sets the provider return code.
	    /// </summary>
	    public string ProviderReturnCode { get; set; }

	    /// <summary>
	    /// Gets or sets the provider return message.
	    /// </summary>
	    public string ProviderReturnMessage { get; set; }

	    /// <summary>
	    /// Gets or sets the links.
	    /// </summary>
	    public List<Link> Links { get; set; } 
	}
}