﻿namespace PaymentCwi.Models
{
    using System;
    using PaymentCwi.Models.Enum;

    /// <summary>
    /// The card.
    /// </summary>
    public class Card
    {
        /// <summary>
        /// Gets or sets the card number.
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Gets or sets the holder.
        /// </summary>
        public string Holder { get; set; }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        public string ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets the security code.
        /// </summary>
        public string SecurityCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether save card.
        /// </summary>
        public bool SaveCard { get; set; }

        /// <summary>
        /// Gets or sets the card token.
        /// </summary>
        public Guid? CardToken { get; set; }

        /// <summary>
        /// Gets or sets the alias.
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Gets or sets the brand.
        /// </summary>
        public BrandEnum Brand { get; set; }
    }
}