﻿namespace PaymentCwi.Models
{
    /// <summary>
    /// The external authentication.
    /// </summary>
    public class ExternalAuthentication
    {
        /// <summary>
        /// Gets or sets the cavv.
        /// </summary>
        public string Cavv { get; set; }

        /// <summary>
        /// Gets or sets the xid.
        /// </summary>
        public string Xid { get; set; }

        /// <summary>
        /// Gets or sets the eci.
        /// </summary>
        public string Eci { get; set; }
    }
}