﻿namespace PaymentCwi.Models
{
    using System.Collections.Generic;
    using System.Net;

    /// <summary>
    /// The base response.
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        /// Gets or sets the http status.
        /// </summary>
        public HttpStatusCode HttpStatus { get; set; }

        /// <summary>
        /// Gets or sets the error data collection.
        /// </summary>
        public List<Error> ErrorDataCollection { get; set; }
    }
}
