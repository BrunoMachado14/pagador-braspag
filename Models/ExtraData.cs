﻿namespace PaymentCwi.Models
{
    /// <summary>
    /// The extra data.
    /// </summary>
    public class ExtraData
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }
    }
}