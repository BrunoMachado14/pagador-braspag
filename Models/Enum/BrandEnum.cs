﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The brand enum.
    /// </summary>
    public enum BrandEnum
    {
        /// <summary>
        /// The undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The visa.
        /// </summary>
        Visa = 1,

        /// <summary>
        /// The master.
        /// </summary>
        Master = 2,

        /// <summary>
        /// The amex.
        /// </summary>
        Amex = 3,

        /// <summary>
        /// The elo.
        /// </summary>
        Elo = 4,

        /// <summary>
        /// The aura.
        /// </summary>
        Aura = 5,

        /// <summary>
        /// The jcb.
        /// </summary>
        Jcb = 6,

        /// <summary>
        /// The diners.
        /// </summary>
        Diners = 7,

        /// <summary>
        /// The discover.
        /// </summary>
        Discover = 8,

        /// <summary>
        /// The hipercard.
        /// </summary>
        Hipercard = 9,

        /// <summary>
        /// The hiper.
        /// </summary>
        Hiper = 10,

        /// <summary>
        /// The naranja.
        /// </summary>
        Naranja = 11,

        /// <summary>
        /// The nevada.
        /// </summary>
        Nevada = 12,

        /// <summary>
        /// The cabal.
        /// </summary>
        Cabal = 13,

        /// <summary>
        /// The credz.
        /// </summary>
        Credz = 14,

        /// <summary>
        /// The cred system.
        /// </summary>
        CredSystem = 15,

        /// <summary>
        /// The banese.
        /// </summary>
        Banese = 16
    }
}