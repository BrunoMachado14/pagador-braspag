﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The provider antifraud enum.
    /// </summary>
    public enum ProviderAntifraudEnum
    {
        /// <summary>
        /// The cybersource.
        /// </summary>
        Cybersource,

        /// <summary>
        /// The clearsale.
        /// </summary>
        Clearsale,

        /// <summary>
        /// The red shield.
        /// </summary>
        RedShield
    }
}