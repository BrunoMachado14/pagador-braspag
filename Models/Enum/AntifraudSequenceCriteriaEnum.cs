﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The antifraud sequence criteria enum.
    /// </summary>
    public enum AntifraudSequenceCriteriaEnum
    {
        /// <summary>
        /// The on success.
        /// </summary>
        OnSuccess = 0,

        /// <summary>
        /// The always.
        /// </summary>
        Always = 1,
    }
}