﻿namespace PaymentCwi.Models.Enum
{
    /// <summary>
    /// The recurrency interval enum.
    /// </summary>
    public enum RecurrencyIntervalEnum
    {
        /// <summary>
        /// The monthly.
        /// </summary>
        Monthly = 1,

        /// <summary>
        /// The bimonthly.
        /// </summary>
        Bimonthly = 2,

        /// <summary>
        /// The quarterly.
        /// </summary>
        Quarterly = 3,

        /// <summary>
        /// The semi annual.
        /// </summary>
        SemiAnnual = 6,

        /// <summary>
        /// The annual.
        /// </summary>
        Annual = 12
    }
}