﻿namespace PaymentCwi.Models.Enum
{
	using ServiceStack.DataAnnotations;

	/// <summary>
	/// The fraud analysis status enum.
	/// </summary>
	[EnumAsInt]
	public enum FraudAnalysisStatusEnum
    {
        /// <summary>
		/// The unknown.
        /// </summary>
		Unknown = 0,

        /// <summary>
		/// The accept.
        /// </summary>
		Accept = 1,


        /// <summary>
        /// The reject.
        /// </summary>
		Reject = 2,

        /// <summary>
		/// The review.
        /// </summary>
		Review = 3,

        /// <summary>
        /// The aborted.
        /// </summary>
		Aborted = 4
    }
}