﻿namespace PaymentCwi.Models.Enum
{
	/// <summary>
	/// The identity type enum.
	/// </summary>
	public enum IdentityTypeEnum
	{
		/// <summary>
		/// The cpf.
		/// </summary>
		CPF,

		/// <summary>
		/// The cnpj.
		/// </summary>
		CNPJ
	}
}