﻿namespace PaymentCwi.Models
{
    /// <summary>
    /// The void request.
    /// </summary>
    public class VoidRequest
    {
        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        public int? Amount { get; set; }
    }
}
