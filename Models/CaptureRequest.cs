﻿namespace PaymentCwi.Models
{
    /// <summary>
    /// The capture request.
    /// </summary>
    public class CaptureRequest
	{
	    /// <summary>
	    /// Gets or sets the amount.
	    /// </summary>
	    public int? Amount { get; set; }

	    /// <summary>
	    /// Gets or sets the service tax amount.
	    /// </summary>
	    public int? ServiceTaxAmount { get; set; }
	}
}
